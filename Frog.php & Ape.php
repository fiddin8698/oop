<?php
    require_once("animal.php");
    class Ape extends Animal {
        public $legs = 2;

        public function yell(){
            echo "Yell : Auooo";
        }
    }

    require_once("animal.php");
    class Frog extends Animal {
        public function jump(){
            echo "Jump : Hop Hop";
        }
    }
    

?>