<?php

    require_once('animal.php');
    require_once('Frog.php & Ape.php');
    $sheep = new Animal("Shaun");
    echo "Name : ". $sheep->name. "<br>";
    echo "Legs : ". $sheep->legs. "<br>";
    echo "Cool Blooded : ". $sheep->cool_blooded. "<br><br>";

    $sungokong = new Ape("Kera Sakti");
    echo "Name : ". $sungokong->name. "<br>";
    echo "Legs : ". $sungokong->legs. "<br>";
    echo "Cool Blooded : ". $sungokong->cool_blooded. "<br>";
    echo $sungokong->yell(). "<br><br>";

    $kodok = new Frog("Buduk");
    echo "Name : ". $kodok->name. "<br>";
    echo "Legs : ". $kodok->legs. "<br>";
    echo "Cool Blooded : ". $kodok->cool_blooded. "<br>";
    echo $kodok->jump(). "<br><br>";

?>
